import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  loginVerify = false;
  connection: any;
  usersOnline: any[] = [];
  usersOnlineSub: any;
  username: string;
  toUser: string;

  constructor(private appService: AppService) {
  }

  ngOnInit() {
    this.getLogin();
    this.appService.connect();
  }

  login() {
    sessionStorage.clear();
    this.appService.login(this.username);
    this.appService.loginResponse().subscribe((data) => {
      sessionStorage.setItem('login-realtime', JSON.stringify(data));
      console.log(data);
      this.getLogin();
    })
  }

  getLogin() {
    if (sessionStorage.getItem('login-realtime')) {
      this.loginVerify = true;
      this.getDataLogin();
    } else {
      this.loginVerify = false;
    }
  }

  getDataLogin() {
    this.usersOnlineSub = this.appService.getUsersOnline().subscribe((res: any) => {
      this.usersOnline = res.users;
    });
    this.getMessage();
  }

  logout() {
    this.appService.logout(JSON.parse(sessionStorage.getItem('login-realtime')));
    this.appService.disconnect();
    sessionStorage.clear();
    this.getLogin();
  }

  sendMessage() {
    this.appService.sendMessage(JSON.parse(sessionStorage.getItem('login-realtime')).id, this.toUser);
  }

  getMessage() {
    this.connection = this.appService
    .getMessages(JSON.parse(sessionStorage.getItem('login-realtime')).id).subscribe((message: any) => {
        alert(`De: ${message.from}. Hola ${message.to}`);
      });
  }

  ngOnDestroy() {
  }
}
