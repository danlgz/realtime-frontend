import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';


@Injectable()
export class AppService {
  private socket;

  constructor() {
  }

  connect() {
    this.socket = io(environment.realtimeApi);
  }

  disconnect() {
    this.socket.disconnect();
  }

  login(username: string) {
    this.socket.emit('login', username);
  }

  loginResponse() {
    let observable = new Observable(observer => {
      this.socket.on('login-response', (data) => {
        observer.next(data);    
      });
      return () => {
        this.socket.disconnect();
      };  
    })     
    return observable;
  }

  logout(user: any) {
    this.socket.emit('logout', user);
  }

  getUsersOnline() {
    let observable = new Observable(observer => {
      this.socket.on('usersonline', (data) => {
        observer.next(data);    
      });
      return () => {
        this.socket.disconnect();
      };  
    })     
    return observable;
  }

  sendMessage(from: string, to: string) {
    this.socket.emit('new-message', {
      to: to,
      from: from
    });
  }

  getMessages(roomUser: string) {
    let observable = new Observable(observer => {
      this.socket = io(environment.realtimeApi);
      this.socket.on(roomUser, (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };  
    })     
    return observable;
  }

}
